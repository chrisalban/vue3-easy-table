import { createApp } from 'vue'
import App from './App.vue'
import EasyTable, { createUI } from 'vue3-easy-table'
import 'vue3-easy-table/themes/default.css'
import es from 'vue3-easy-table/src/lang/es'

createUI({
    translation: es
})

createApp(App).use(EasyTable).mount('#app')
