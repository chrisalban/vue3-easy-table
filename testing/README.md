# Vue 3 Easy Table

A simple and highly customizable component for creating data tables based on plugins.

![preview](https://gitlab.com/chrisalban/vue3-easy-table/-/raw/master/preview.png)

[View demo](https://codesandbox.io/s/sad-fermat-4vwm9)

## Installation

### NPM

```bash
npm install vue3-easy-table --save
```

## Usage

### Register globally

First, register the full components in your app entry point (`app.js`) .

```typescript
import { createApp} from 'vue'
import App from './App.vue'
import EasyTable from 'vue3-easy.table'
...
createApp(App)
    .use(EasyTable)
    .mount('#app')
```

Or also you can register components individually.

```tsx
import { createApp} from 'vue'
import App from './App.vue'
import {
    useEasyDataTable,
    useEasyPaginator,
    useEasySortFilter,
    useEasyTotalizer
} from 'vue3-easy.table'

createApp(App)
    .use(useEasyDataTable)
    .use(useEasyPaginator)
    .use(useEasySortFilter)
    .use(useEasyTotalizer)
    .mount('#app')
...
```

Next, use it on your template

#### Basic Example

```vue
<template>
	<div class="overflow-x-auto w-full">
        <e-data-table
          :headers="headers"
          :items="data"
          :is-loading="loading">
        </e-data-table>
	</div>
</template>
<script>
export default {
    data() {
        return {
            headers: [
                { label: 'Id', key: 'id'},
                { label: 'Username', key: 'username'},
                { label: 'Full Name', key: 'fullName'},
                { label: 'Amount', key: 'amount', formatter: (value) => `$${value}` },
            ],
            items: [
                { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
                { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
                { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
                { id: 4, username: 'sandycheeks', fullName: 'Sandy Cheeks', amount: 800 },
                { id: 5, username: 'eugenekrabs', fullName: 'Eugene H. Krabs', amount: 950 },
                { id: 6, username: 'sheldonplankton', fullName: 'Sheldon J. Plankton', amount: 300 },
            ],
            isLoading: false
        }
    }
}
</script>

<style>
</style>
```

#### Advanced Example

```vue
<template>
    <div>
        <e-keyword-filter v-model:items="items"></e-keyword-filter>
        <e-data-table
            :headers="headers"
            :items="items"
            :is-loading="isLoading"
            :current-page="currentPage"
            v-model:total-items="totalItems"
            :per-page="perPage"
            rowStyle="link"
            @row-click="showUser">

            <template v-slot:header[default]="{ header }">
                {{ header.label }}
                <e-sort-filter :key-param="header.key" v-model:sort-by="sortBy" v-model:sort-desc="sortDesc" v-model:items="items"/>
            </template>
            <template v-slot:cell[username]="{ cell }">
                <div class="flex items-center">
                    <span>{{ `${cell.item.username}` }}</span>
                </div> 
            </template>
            <template v-slot:footer[amount]="{ footer }">
                <e-totalizer :key-column="footer.key" :items="items" :formatter="footer.formatter"/>
            </template>
        </e-data-table>
        <div class="mt-auto">
            <e-paginator v-model:current-page="currentPage" v-model:total-items="totalItems" v-model:per-page="perPage" />
        </div>
    </div>
</template>

<script>
export default {
    data() {
        return {
            headers: [
                { label: 'Id', key: 'id'},
                { label: 'Username', key: 'username'},
                { label: 'Full Name', key: 'fullName'},
                { label: 'Amount', key: 'amount', formatter: (value) => `$${value}` },
            ],
            items: [
                { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
                { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
                { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
                { id: 4, username: 'sandycheeks', fullName: 'Sandy Cheeks', amount: 800 },
                { id: 5, username: 'eugenekrabs', fullName: 'Eugene H. Krabs', amount: 950 },
                { id: 6, username: 'sheldonplankton', fullName: 'Sheldon J. Plankton', amount: 300 },
            ],
            isLoading: false,
            perPage: 3,
            totalItems: 0,
            currentPage: 1,
            sortBy: 'username',
            sortDesc: false,
        }
    },
    methods: {
        showUser(user) {
            alert(user.fullName)
        }
    }
}
</script>

<style>
</style>
```

### Configuring the components

Vue3 Easy Table includes components that offer a large number of properties for your configuration.

#### Data Table

The data table is the main component that displays a table with the data that it receives by parameter.

##### Props

###### *headers*

Sets an object that allows you to configure the table headers.

*type:*`Array`

*required:*`true`

```javascript
/*
* You can configure each header individually.
* label: The label to be displayed in the column header
* username: The name of the item's parameter
* formatter: Optionally you can include a function that formats the value that is passed to the cell
* width: Optinally you can indicate the width of the column on a string, ex: '20%', '50px', '2.5rem'
* aligment: Optionally you can indicate de column aligment on a string, ex: 'left', 'center', 'right'
*/
[
    { label: 'Id', key: 'id', width: '20px'},
    { label: 'Userbaname', key: 'username'},
    { label: 'Full Name', key: 'fullName'},
    { label: 'Amount', key: 'amount', formatter: (value) => `$${value}`, aligment: 'right' },
]

```

###### *row-style*

Specify the style that the column will have (`none`, `link`) and they can be customized through the theme styles.

*type:* `String`

*default:* `"none"`

```javascript
rowStyle: 'link'
```

###### *items*

It is the collection of data that will be iterated in the table.

*type:* `Array`

*required:* `true`

```javascript
[
    { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
    { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
    { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
    { id: 4, username: 'sandycheeks', fullName: 'Sandy Cheeks', amount: 800 },
    { id: 5, username: 'eugenekrabs', fullName: 'Eugene H. Krabs', amount: 950 },
    { id: 6, username: 'sheldonplankton', fullName: 'Sheldon J. Plankton', amount: 300 },
]
```

###### *is-loading*

Allows you to specify if the table is in loading state or not.

*type:* `Boolean`

*default:* `false`

```javascript
isLoading: true
```

###### *current-page*

Indicates the current page number where the pagination is located.

*type:* `Number`

*default:* `1`

```javascript
currentPage: 5 // Show de items in the page 5
```

###### *per-page*

Indicates the number of pages to be displayed in the table.

*type:* `Number`

*default*: `15`

```javascript
perPage: 15
```
##### Events

`@row-clicked`

The event is fired when a row is clicked.

##### Slots

You can customize the content within the headers, cells, feet, through the slots.

To refer to a specific slot you can use the syntax:

```vue
v-slot: header [headerKey]
v-slot: cell [headerKey]
v-slot: footer [headerKey]
```

Without wanting to set a slot for everyone you can use the default slot that will be mounted by default

```vue
v-slot: header [default]
v-slot: cell [default]]
v-slot: footer [default]
```

```vue
<!-- Header template -->
<template v-slot:header[default]="{ header }">
	{{ header.label }}
	<e-sort-filter :key-param="header.key" v-model:sort-by="sortBy" v-model:sort-desc="sortDesc" v-model:items="items"/>
</template>
<!-- Cell template -->
<template v-slot:cell[username]="{ cell }">
	<div class="flex items-center">
        <span>{{ `${cell.item.username}` }}</span>
    </div> 
</template>
<!-- Footer template -->
<template v-slot:footer[amount]="{ footer }">
	<e-totalizer :key-column="footer.key" :items="items" :formatter="footer.formatter"/>
</template>
```

##### Theming

You can fully customize the plugin with the classes it includes.

```css
.et-container {
    width: 100%;
    border-style: solid;
    border-width: 1px;
    border-radius: .8rem;
    border-color: #191919;
    overflow: hidden;
}

.et-container .et-table {
    position: relative;
    width: 100%;
    border-collapse: collapse;
}

.et-container .et-table.loading {
    opacity: 50%;
}

.et-container .et-table-header {
    position: sticky;
    top: 0;
    background-color: #e4e3e5; 
}

.et-container .et-table-header-container {
    display: flex;
    padding: 0.2rem 1rem;
    height: 100%;
    justify-content: center;
    align-items: center;
    padding: .2rem 2rem;
}

.et-container .et-table-cell {
    font-size: 1rem;
    text-align: center;
}

.et-container .et-table-body-row.et-table-row-link {
    cursor: pointer;
}

.et-container .et-table-body-row.et-table-row-link:hover {
    background-color: #b5b5b5;
    box-shadow: 0 0 0 0 #333333;
    transition: all .3s ease;
}

.et-container .et-table-cell-container {
    display: flex;
    padding: 1rem 2rem;
}
```

#### Keyword Filter

Plugin that allows you to filter the items by a keyword.

##### Props

###### *items*

Collection of data that the filter will sort, keep in mind that the filter will modify the original collection.

*type:* `Array`

*required:* `true`

```javascript
[
    { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
    { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
    { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
]
```
###### *placeholder*

The text to display in the placeholder.

*type:* `String`

*default:* `Search`

```javascript
placeholder: 'Search item'
```
##### Theming

You can fully customize the plugin with the classes it includes.

```css
.et-keyword {
    padding: .5rem;
}

.et-keyword .et-keyword-input
{
    padding: .5rem;
    border-radius: .5rem;
    border: none;
}
```

#### Sort Filter

Plugin that allows you to sort the items within the array by a given key in ascending or descending order.

##### Props

###### *items*

Collection of data that the filter will sort, keep in mind that the filter will modify the original collection.

*type:* `Array`

*required:* `true`

```javascript
[
    { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
    { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
    { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
]
```
###### *key-param*

The key of the current column on which the plugin will act.

*type:* `String`

*required:* `true`

```javascript
keyParam: 'username'
```
###### *sort*-by

The key of the column by which the plugin should sort.

*type:* `String`

*required:* `true`

```javascript
sortBy: 'username'
```
###### *sort-desc*

Direction in which the items should be ordered.

*type:* `Boolean`

*required:* `true`

```javascript
sortDesc: true
```

##### Theming

You can fully customize the plugin with the classes it includes.

```css
.et-sorter
{
    display: flex;
    justify-content: center;
    margin-left: 1rem;
    margin: 0.2rem 1rem;
    border-radius: .3rem;
    transition: all .3s ease;
    border: none;
    padding: .2rem .5rem;
}

.et-sorter:hover,
.et-sorter:active
{
    background-color: #b5b5b5;
}

.et-sorter.neutral
{
    width: 100%;
    color: #333333;
}
```

#### Paginator

Plugin that allows you to control the pagination of a table.

##### Props

###### *current-page*

Indicates the current page that the component should show as marked.

*type:* `Number`

*default:* `1`

```javascript
currentPage: 1
```
###### *total-items*

The total number of items in the data collection, this data is used so that the plugin can calculate the number of pages.

*type:* `Number`

*required:* `true`

```javascript
totalItems: 150
```
###### *per-page*

The number of items displayed per page, the plugin uses this data to calculate the number of pages.

*type:* `Number`

*required:* `true`

```javascript
perPage: 15
```
###### *offset*

The maximum number of pages to be displayed.

*type:* `Boolean`

*default:* `5`

```javascript
offset: 5
```

##### Theming

You can fully customize the plugin with the classes it includes.

```css
.et-paginator
{
    display: flex;
    flex-direction: row-reverse;
}

.et-paginator .et-paginator-container
{
    display: flex;
    margin: 1rem auto;
    border-width: 1px;
    border-style: solid;
    border-color:#333333; 
    border-radius: .3rem;
    overflow: hidden;
}

.et-paginator .nav-button
{
    padding: .2rem 1rem;
    border-right: 1px solid #333333;
    outline: none;
    background-color: transparent;
}

.et-paginator .nav-button.pre
{
    padding: .2rme 1rem;
    border-right: 1px solid #333333;
    outline: none
}

.et-paginator .nav-button.next
 {
     border: none;
}

.et-paginator .nav-button:hover {
    background-color: #b5b5b5;
}

 .et-paginator .nav-button.active {
    background-color: #05308c;
    color: white;
    border: 1px solid #05308c;
}
```
#### Totalizer

Complement that calculates the total, of a column, keep in mind that if there are other filters applied, all the filtered items will be added.

##### Props

###### *key-column*

The key of the column to be totaled.

*type:* `String`

*required:* `true`

```javascript
keyColumn: 'amount'
```
###### *items*

The filtered data collection.

*type:* `Array`

*required:* `true`

```javascript
[
    { id: 1, username: 'Octavius Rex', fullName: 'Octavius Rex', amount: 1000 },
    { id: 2, username: 'spongebob', fullName: 'Sponge Bob Squarepants', amount: 1500 },
    { id: 3, username: 'patrickstar', fullName: 'Patrick Star', amount: 1020 },
]
```
###### *formatter*

The formatting function that will be applied to the footer.

*type:* `Function`

*required:* `false`

```javascript
formatter: (value) => value.toLowerCase()
```

##### Theming

You can fully customize the plugin with the classes it includes.

```css
.et-totalizer {
    font-weight: bold;
    text-align: center;
}
```