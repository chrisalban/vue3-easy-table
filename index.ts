import DataTable from './src/components/vue-easy-table/data-table/DataTable.vue'
import Paginator from './src/components/vue-easy-table/paginator/Paginator.vue'
import SortFilter from './src/components/vue-easy-table/filters/sort-filter/SortFilter.vue'
import Totalizer from './src/components/vue-easy-table/totalizer/Totalizer.vue'
import KeywordFilter from './src/components/vue-easy-table/filters/keyword-filter/KeywordFilter.vue'
import PageNumber from './src/components/vue-easy-table/page-number/PageNumber.vue'
import { default as CreatedAppExport, wordTranslation as wordTranslationExport, createUI as createUIExport, changeLang as changeLangExport } from './src/app'
import {App as VueApp} from 'vue'

export default {
    install(app: any) {
        app.component('e-data-table', DataTable)
        app.component('e-paginator', Paginator)
        app.component('e-sort-filter', SortFilter)
        app.component('e-keyword-filter', KeywordFilter)
        app.component('e-totalizer', Totalizer)
        app.component('e-page-number', PageNumber)
    }
}

export const CreatedApp = CreatedAppExport

export const createUI = createUIExport

export const wordTranslation = wordTranslationExport

export const changeLang = changeLangExport

export const useEasyDataTable = {
    install(app: VueApp) {
        app.component('e-data-table', DataTable)
    }
}

/* export const EDataTable = DataTable */

export const useEasyPaginator = {
    install(app: VueApp) {
        app.component('e-paginator', Paginator)
    }
}

/* export const EPaginator = Paginator */

export const useEasySortFilter = {
    install(app: VueApp) {
        app.component('e-sort-filter', SortFilter)
    }
}

/* export const ESortFilter = SortFilter */

export const useEasyKeywordFilter = {
    install(app: VueApp) {
        app.component('e-keyword-filter', KeywordFilter)
    }
}

/* export const EKeywordFilter = KeywordFilter */

export const useEasyTotalizer = {
    install(app: VueApp) {
        app.component('e-totalizer', Totalizer)
    }
}

/* export const ETotalizer = Totalizer */

export const useEasyPageNumber = {
    install(app: VueApp) {
        app.component('e-page-number', PageNumber)
    }
}

/* export const EPageNumber = PageNumber */
