import { GlobalSettings } from './types'

class App
{
    constructor(
        protected globalSettings: GlobalSettings = {
            translation: {}
        }
    ) {}

    lang(translation: any) {
        this.globalSettings.translation = translation 
    }

    wordTranslation(word: string): string | undefined {
        return this.globalSettings.translation[word]
    }
}

let createdApp: App|undefined = undefined

export default createdApp

export const createUI = (settins?: GlobalSettings) => {
    createdApp = new App(settins)
}

const updateSetting = (helper: any, ...params: any[]): any => {
    if(createdApp) {
        const func = helper.bind(createdApp)
        return func(...params)
    }
}

export const wordTranslation = (word: string): string => updateSetting(createdApp?.wordTranslation, word) ?? word

export const changeLang = (lang: any) => updateSetting(createdApp?.lang, lang)
