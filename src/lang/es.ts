export default {
  'records':    'registros',
  'of':         'de',
  'page':       'página',
  'Search':     'Buscar',
  'Empty':      'Sin Registros'
}
