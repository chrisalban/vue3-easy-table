export interface TableDetailHeader<Item> {
    label: string, 
    key: string, 
    aligment?: string,
    width?: string,
    formatter?(formatter: Item): string,
}

export interface TableDetailCell<Item> {
    header: TableDetailHeader<Item>,
    data: string|number,
    item: Item,
}

export enum TableRowStyle {
    NONE = 'none',
    LINK = 'link',
}

export interface TableDetailProps<Item> {
    headers: TableDetailHeader<Item>[],
    rowStyle: TableRowStyle,
    items: Item[],
    isLoading: boolean,
    currentPage: number,
    perPage: number,
    sortBy: string,
    sortDesc: boolean
    filterBy: string
}

export interface TotalizerProps<Item>{
    keyColumn: string,
    items: Item[]
    formatter?(formatter: string|number): string,
}

export interface FilterProps<Item>{
    items: Item[]
}

export interface GlobalSettings
{
    translation: any
}

export enum TableCellAligment {
    ALIGN_LEFT = 'left',
    ALIGN_CENTER = 'center',
    ALIGN_RIGHT = 'right',
}
