import {computed, ref} from "vue"
import { wordTranslation } from '../../../app'

export const usePageNumber = (props: any) => {
    const pages = computed(() => Math.ceil(props.totalItems / props.perPage))
 
    const totalItemsLabel = ref(wordTranslation('records'))

    const pageSeparatorLabel = ref(wordTranslation('of'))

    const translatedPage = ref(wordTranslation('page'))

    return { pages, totalItemsLabel, pageSeparatorLabel, translatedPage }
}
