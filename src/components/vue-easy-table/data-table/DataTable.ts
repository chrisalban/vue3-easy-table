import { wordTranslation } from '../../../app'
import { computed, ref } from 'vue'
import { TableDetailCell, TableDetailHeader} from '../../../types'

export const useDataTable = (props: any, { emit }: any) => {
    const items = computed(<Item>() => {
        const rows: TableDetailCell<Item>[][] = [] 

        props.items
        .forEach((item: Item) => {
            const columns: TableDetailCell<Item>[] = []

            props.headers.forEach((header: TableDetailHeader<Item>) => {
                const itemValue: any = item
                const value: (string|number) = header.formatter ? header.formatter(itemValue[header.key]) : itemValue[header.key]
                columns.push({
                    header: header,
                    data: value,
                    item,
                })
            })

            rows.push(columns)
        })

        emit('update:total-items', rows.length)

        return rows
    })

    const rows = computed(() => {
        const to = (props.perPage * props.currentPage)
        const from = to - props.perPage
        return items.value.slice(from, to)
    })

    const isExpanded = ref(false)  

    const showItem = <Item>(item: Item) => {
        emit('row-click', item)
    }

    const translatedEmpty = ref(wordTranslation(props.emptyLabel))

    return {items, rows, isExpanded, showItem, translatedEmpty }
}
