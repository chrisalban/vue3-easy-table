import {computed} from "@vue/runtime-core"
import {TotalizerProps} from "../../../types"

export const toCurrency = (value: number) => (value).toLocaleString('en-US', { style: 'currency', currency: 'USD' })

export const useTotalizer = <Item>(props?: TotalizerProps<Item>) => {

    const totalize = (items: Item[], keyColumn: string): number => {
        return items.reduce((counter: number, item: any) => counter + item[keyColumn], 0)
    }

    const total = computed(() => {
        if(!props?.items)
            return 0

        if(props.formatter)
            return props.formatter(totalize(props.items, props.keyColumn))

        return totalize(props.items, props.keyColumn)
    })

    return { total, totalize }
}
