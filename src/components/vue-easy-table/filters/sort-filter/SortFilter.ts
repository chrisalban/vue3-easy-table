import { computed, ref, watch } from "@vue/runtime-core"

export const sortItems = (items: any[], sortBy: string, sortDesc: boolean = true): any[] => {
    return items.sort((a: any, b: any) => {
        if(sortBy === '')
            return 0

        const objectA: string | number = typeof a[sortBy] === 'number' ? a[sortBy] : `${a[sortBy]}`.toLowerCase()
        const objectB: string | number = typeof b[sortBy] === 'number' ? b[sortBy] : `${b[sortBy]}`.toLowerCase()

        let evaluation = 0
        if(objectA < objectB)
            evaluation = -1
        if(objectA > objectB)
            evaluation = 1

        return sortDesc ? evaluation * -1 : evaluation
    })
}

export const useSortFilter = (props: any, { emit }: any) => { 

    const toggleSort = () => {
        let sortDesc = true

        if(props.sortBy === props.keyParam)
            sortDesc = !props.sortDesc
        
        emit('update:sort-by', props.keyParam) 
        
        emit('update:sort-desc', sortDesc)

        emit('update:filtered', sortItems(props.filtered, props.keyParam, sortDesc))
    }

    watch(props.filtered, (value: any[]) => emit('update:filtered', sortItems(value, props.sortBy, props.sortDesc)))

    return { toggleSort }
}
