import { wordTranslation } from '../../../../app'
import {computed, onMounted, ref, watch} from "vue"

export const useKeywordFilter = (props: any, { emit }: any) => {
    const keywordValue = ref(props.keyword)

    const keyword = computed(() => props.keyword)

    const filteredItems = computed(() => {
        return props.original.filter(<Item>(item: Item) => {
            let hasValue: boolean = false

            for(const key in item ) {
                if((`${item[key]}`.toLowerCase().indexOf(keywordValue.value.toLowerCase()) >= 0) || keywordValue.value === '') {
                    hasValue = true
                    break
                } 
            }

            return hasValue 
        })
    })

    const updateFiltered = () => emit('update:filtered', filteredItems.value)

    const translatedPlaceholder = ref(wordTranslation(props.placeholder))

    watch(filteredItems, () => updateFiltered()) 

    watch(filteredItems, () => updateFiltered()) 

    watch(keywordValue, (keyword) => emit('update:keyword', keyword))

    watch(keyword, (value) => keywordValue.value = value )

    onMounted(() => updateFiltered())

    return { keywordValue, filteredItems, translatedPlaceholder }
}
