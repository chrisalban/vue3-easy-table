import { computed } from 'vue'

export const usePaginator = (props: any, { emit }: any) => {
    const totalPages = computed(() => {
        emit('update:current-page', 1)
        return Math.ceil(props.totalItems / props.perPage)
    })

    const previousPage = () => {
        if(props.currentPage > 1)
            emit('update:current-page', props.currentPage - 1)
    }

    const nextPage = () => {
        if(props.currentPage < totalPages.value)
            emit('update:current-page', props.currentPage + 1)
    }

    const gotToPage = (page: number) => {
        emit('update:current-page', page)
    }

    let firstPage = 0
    let lastPage = firstPage + props.offset

    const pages = computed(() => {
        const pages: number[] = []
        for(let i = 1; i <= totalPages.value; i++) {
            pages.push(i)
        } 

        if(props.currentPage <= firstPage && firstPage - 1 >= 0) {
            firstPage--
            lastPage--
        } 

        if(props.currentPage > lastPage && lastPage + 1 <= totalPages.value) {
            firstPage++
            lastPage++
        } 

        return pages.slice(firstPage, lastPage)
    }) 

    return { previousPage, nextPage, pages, gotToPage, totalPages }
}
